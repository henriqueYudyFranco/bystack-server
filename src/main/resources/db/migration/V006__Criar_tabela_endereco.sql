CREATE TABLE endereco(
	id_endereco 						SERIAL 			PRIMARY KEY,
	bairro				VARCHAR(200)	NOT NULL,
	logradouro			VARCHAR(200)	NOT NULL,
	numero				INTEGER 		NOT NULL,
	cidade				VARCHAR(200)	NOT NULL
);

insert into endereco values (1, 'Eneias' ,'Rua Jose Martins Gallego', 420, 'Patrocinio');
insert into endereco values (2, 'Centro' ,'Av. Faria Pereira', 1090, 'Patrocinio');
insert into endereco values (3, 'Centro' ,'Av. Rui Barbosa', 324, 'Patrocinio');
insert into endereco values (4, 'Centro' ,'Av Governador Valadares', 3456, 'Patrocinio');
insert into endereco values (5, 'Centro' ,'Rua Bernado Guimarães', 3467, 'Patrocinio');
insert into endereco values (6, 'Constantino' ,'Av. João Alves do Nascimento', 2786, 'Patrocinio');