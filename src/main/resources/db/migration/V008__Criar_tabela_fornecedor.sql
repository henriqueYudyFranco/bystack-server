CREATE TABLE fornecedor(
	id_fornecedor 		SERIAL 			PRIMARY KEY,
	nome_fantasia		VARCHAR(200)	NOT NULL,
	razao_social		VARCHAR(200)	NOT NULL,
	cnpj			    VARCHAR(18)		NOT NULL,
	telefone			VARCHAR(20)		NOT NULL,
	endereco			bigint			NOT NULL,
	constraint fk_fornecedor_cliente foreign key (endereco) references endereco (id_endereco)
);

insert into fornecedor values (1, 'OnNet Telecom' , 'OnNet Telecom LTDA ME', '1523156100001', '3498798754', 2);
insert into fornecedor values (2, 'Quejaria' , 'Fabrica Queijado', '54566540001', '3498798754', 2);
