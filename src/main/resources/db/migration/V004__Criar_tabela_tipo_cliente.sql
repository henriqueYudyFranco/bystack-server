CREATE TABLE tipo_cliente(
	id_tipo_cliente 			SERIAL 			PRIMARY KEY,
	descricao					VARCHAR(100)	NOT NULL
);

insert into tipo_cliente values (1, 'fisico');
insert into tipo_cliente values (2, 'juridico');