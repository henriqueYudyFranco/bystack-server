CREATE TABLE cliente(
	id 					SERIAL 			PRIMARY KEY,
	nome				VARCHAR(100)	NOT NULL,
	data_nascimento		Date			NOT NULL,
	cpf					VARCHAR(18)		NOT NULL,
	telefone			VARCHAR(20)		NOT NULL,
	tipo_cliente		bigint			NOT NULL,
	endereco			bigint			NOT NULL,
	constraint fk_cliente_endereco foreign key (endereco) references endereco (id_endereco),
	constraint fk_cliente_tipo_cliente foreign key (tipo_cliente) references tipo_cliente (id_tipo_cliente)
);


insert into cliente values (1, 'Henrique Yudy Franco', '24-12-1996', '11111111111', '34999115799', 1, 1);
insert into cliente values (2, 'Gustavo Pereira', '10-11-1995', '22222222222', '489456123', 1, 1);
insert into cliente values (3, 'Guilherme Pereira', '14-09-1996', '33333333333', '34999115799', 1, 1);
insert into cliente values (4, 'Felipe Araujo', '24-12-1996', '44444444444', '34999115799', 1, 1);
insert into cliente values (5, 'Aline Barbosa', '24-12-1996', '55555555555', '34999115799', 1, 1);