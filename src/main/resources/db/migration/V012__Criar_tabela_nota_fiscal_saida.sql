CREATE TABLE nota_fiscal_saida(
	id_nota_fiscal_saida 			SERIAL 			PRIMARY KEY,
	id_movimentacao_saida		    bigint			NOT NULL,
	data_nota_fiscal				Date			NOT NULL,
	valor							Decimal			NOT NULL,
	constraint fk_nota_saida_mov foreign key (id_movimentacao_saida) references movimentacao_saida (id_movimentacao_saida)
);
