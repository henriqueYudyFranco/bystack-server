CREATE TABLE movimentacao_saida(
	id_movimentacao_saida 			SERIAL 			PRIMARY KEY,
	quantidade		   			    INTEGER			NOT NULL,
	desconto						DECIMAL			NOT NULL,
	valor_pgto			   			DECIMAL			NOT NULL,
	valor_total						DECIMAL			NOT NULL,
	data_movimentacao				DATE			NOT NULL,
	produto_preco					bigint			NOT NULL,
	cliente							bigint			NOT NULL,
	constraint fk_mov_saida_produto foreign key (produto_preco) references produto_preco (id_produto_preco),
	constraint fk_mov_saida_cliente foreign key (cliente) references cliente (id)

)
