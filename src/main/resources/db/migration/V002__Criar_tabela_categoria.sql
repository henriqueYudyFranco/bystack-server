CREATE TABLE categoria(
	id_categoria 		SERIAL 			PRIMARY KEY,
	nome				VARCHAR(100)	NOT NULL
);


INSERT INTO categoria values (1, 'Eletronicos');
INSERT INTO categoria values (2, 'Telefones');
INSERT INTO categoria values (3, 'Computadores');
INSERT INTO categoria values (4, 'Brinquedos');
INSERT INTO categoria values (5, 'Auto e Moto');
INSERT INTO categoria values (6, 'Moda Feminina');
INSERT INTO categoria values (7, 'Moda Masculina');
INSERT INTO categoria values (8, 'Joias');