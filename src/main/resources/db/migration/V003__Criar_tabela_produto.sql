CREATE TABLE produto(
	id_produto 			SERIAL 			PRIMARY KEY,
	nome				VARCHAR(100)	NOT NULL,
	id_categoria		bigint			NOT NULL,
	cod_barra			int				NOT NULL,
	quantidade			int				NOT NULL,
	constraint fk_categoria_produto foreign key (id_categoria) references categoria (id_categoria)
);


INSERT INTO produto values (1, 'Notebook Dell i5', 3, 200, 1 );
INSERT INTO produto values (2, 'Notebook ASUS i7', 3, 201, 1 );
INSERT INTO produto values (3, 'Notebook ACER i3', 3, 202, 1 );

INSERT INTO produto values (4, 'USB Carregador 12v', 1, 10, 1 );
INSERT INTO produto values (5, 'Drone Genex', 1, 11, 1 );
INSERT INTO produto values (6, 'Fones Samsung', 1, 13, 1 );