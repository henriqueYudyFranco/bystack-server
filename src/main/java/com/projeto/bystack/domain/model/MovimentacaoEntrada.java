package com.projeto.bystack.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "movimentacao_entrada")
public class MovimentacaoEntrada {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_movimentacao_entrada")
	private long id;

	@Column(name = "quantidade")
	private int quantidade;

	@Column(name = "valor_total")
	private BigDecimal valor_total;

	@Column(name = "data_movimentacao")
	private LocalDate data_movimentacao;

	@ManyToOne
	@JoinColumn(name = "produto")
	private Produto produto;

	@ManyToOne
	@JoinColumn(name = "fornecedor")
	private Fornecedor fornecedor;

	public long getId() {
		return id;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public BigDecimal getValor_total() {
		return valor_total;
	}

	public LocalDate getData_movimentacao() {
		return data_movimentacao;
	}

	public Produto getProduto() {
		return produto;
	}

	public Fornecedor getFornecedor() {
		return fornecedor;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public void setValor_total(BigDecimal valor_total) {
		this.valor_total = valor_total;
	}

	public void setData_movimentacao(LocalDate data_movimentacao) {
		this.data_movimentacao = data_movimentacao;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public void setFornecedor(Fornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@Override
	public String toString() {
		return "MovimentacaoEntrada [id=" + id + ", quantidade=" + quantidade + ", valor_total=" + valor_total
				+ ", data_movimentacao=" + data_movimentacao + ", produto=" + produto + ", fornecedor=" + fornecedor
				+ "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MovimentacaoEntrada other = (MovimentacaoEntrada) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
