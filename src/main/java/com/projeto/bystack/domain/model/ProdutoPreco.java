package com.projeto.bystack.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "produto_preco")
public class ProdutoPreco {

	@Id()
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_produto_preco")
	private long id;

	@Column(name = "data_inicio")
	private LocalDate data_inicio;

	@Column(name = "data_fim")
	private LocalDate data_fim;

	@Column(name = "preco")
	private BigDecimal preco;

	@ManyToOne
	@JoinColumn(name = "produto")
	private Produto produto;

	public long getId() {
		return id;
	}

	public LocalDate getData_inicio() {
		return data_inicio;
	}

	public LocalDate getData_fim() {
		return data_fim;
	}

	public BigDecimal getPreco() {
		return preco;
	}

	public Produto getProduto() {
		return produto;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setData_inicio(LocalDate data_inicio) {
		this.data_inicio = data_inicio;
	}

	public void setData_fim(LocalDate data_fim) {
		this.data_fim = data_fim;
	}

	public void setPreco(BigDecimal preco) {
		this.preco = preco;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	@Override
	public String toString() {
		return "ProdutoPreco [id=" + id + ", data_inicio=" + data_inicio + ", data_fim=" + data_fim + ", preco=" + preco
				+ ", produto=" + produto + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProdutoPreco other = (ProdutoPreco) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
