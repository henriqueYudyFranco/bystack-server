package com.projeto.bystack.domain.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "nota_fiscal_entrada")
public class NotaFiscalEntrada {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_nota_fiscal_entrada") 
	private long id_nota_fiscal_entrada;
	
	@ManyToOne
	@JoinColumn(name= "id_movimentacao_entrada")
	private MovimentacaoEntrada id_movimentacao_entrada;
	
	
	@Column(name = "data_nota")
	private LocalDate data_nota;
	
	@Column(name = "valor")
	private BigDecimal valor;

	public long getId_nota_fiscal_entrada() {
		return id_nota_fiscal_entrada;
	}

	public MovimentacaoEntrada getId_movimentacao_entrada() {
		return id_movimentacao_entrada;
	}

	public LocalDate getData_nota() {
		return data_nota;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setId_nota_fiscal_entrada(long id_nota_fiscal_entrada) {
		this.id_nota_fiscal_entrada = id_nota_fiscal_entrada;
	}

	public void setId_movimentacao_entrada(MovimentacaoEntrada id_movimentacao_entrada) {
		this.id_movimentacao_entrada = id_movimentacao_entrada;
	}

	public void setData_nota(LocalDate data_nota) {
		this.data_nota = data_nota;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	@Override
	public String toString() {
		return "NotaFiscalEntrada [id_nota_fiscal_entrada=" + id_nota_fiscal_entrada + ", id_movimentacao_entrada="
				+ id_movimentacao_entrada + ", data_nota=" + data_nota + ", valor=" + valor + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id_nota_fiscal_entrada ^ (id_nota_fiscal_entrada >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NotaFiscalEntrada other = (NotaFiscalEntrada) obj;
		if (id_nota_fiscal_entrada != other.id_nota_fiscal_entrada)
			return false;
		return true;
	}

}
