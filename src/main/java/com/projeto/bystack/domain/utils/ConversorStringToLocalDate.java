package com.projeto.bystack.domain.utils;

import java.time.LocalDate;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class ConversorStringToLocalDate implements AttributeConverter<LocalDate, String>{

	@Override
	public String convertToDatabaseColumn(LocalDate locDate) {
		return (locDate == null ? null : String.valueOf(locDate));
	}

	@Override
	public LocalDate convertToEntityAttribute(String dbData) {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
