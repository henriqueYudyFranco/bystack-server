package com.projeto.bystack.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.bystack.domain.model.ProdutoPreco;
import com.projeto.bystack.domain.repository.Query.ProdutoPrecoRepositoryQuery;

public interface ProdutoPrecoRepository extends JpaRepository<ProdutoPreco, Long>, ProdutoPrecoRepositoryQuery{

}
