package com.projeto.bystack.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.projeto.bystack.domain.model.NotaFiscalSaida;

@Repository
public interface NotaFiscalSaidaRepository extends JpaRepository<NotaFiscalSaida, Long>{

}
