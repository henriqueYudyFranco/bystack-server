package com.projeto.bystack.domain.repository.Impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;

import com.projeto.bystack.domain.model.Categoria;
import com.projeto.bystack.domain.model.ProdutoPreco;
import com.projeto.bystack.domain.repository.Query.ProdutoPrecoRepositoryQuery;

public class ProdutoPrecoRepositoryImpl implements ProdutoPrecoRepositoryQuery {

	@Autowired
	private EntityManager manager;

	@Override
	public List<ProdutoPreco> produtoPorCategoria(Categoria categoria) {

		StringBuilder consulta = new StringBuilder();

		consulta.append("SELECT pp FROM ProdutoPreco pp , Produto p , Categoria c ")
				.append("WHERE pp.produto = p.id AND p.categoria = c.id  AND  p.categoria = :categoria ");

		try {
			return manager.createQuery(consulta.toString(), ProdutoPreco.class).setParameter("categoria", categoria)
					.getResultList();
		} catch (NoResultException e) {

			return null;
		}
	}

	@Override
	public ProdutoPreco produtoQrcode(int code) {
		
		StringBuilder consulta = new StringBuilder();
		
		consulta.append("SELECT pp FROM ProdutoPreco pp , Produto p WHERE pp.produto = p.id AND p.cod_barra = :code ");
		
		
		try {
			return manager.createQuery(consulta.toString() , ProdutoPreco.class)
						 .setParameter("code", code)
						 .getSingleResult();
					
		}catch (NoResultException e) {
			return null;
		}
	}

}
