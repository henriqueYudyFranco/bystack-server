package com.projeto.bystack.domain.repository.Impl;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;

import com.projeto.bystack.domain.model.Cliente;
import com.projeto.bystack.domain.repository.Query.ClienteRepositoryQuery;

public class ClienteRepositoryImpl implements ClienteRepositoryQuery{
	
	@Autowired
	private EntityManager manager;

	@Override
	public Cliente buscarPorCpf(String cpf) {

		StringBuilder consulta = new StringBuilder();
		
		consulta.append("SELECT c FROM Cliente c WHERE c.cpf = :cpf ");
		
		try {
			return manager.createQuery(consulta.toString(), Cliente.class)
				   .setParameter("cpf", cpf)
				   .getSingleResult();
		}catch (NoResultException e) {
			return null;
		}
	}

}
