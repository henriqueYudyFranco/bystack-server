package com.projeto.bystack.domain.repository.Query;

import java.util.List;

import com.projeto.bystack.domain.model.Categoria;
import com.projeto.bystack.domain.model.ProdutoPreco;


public interface ProdutoPrecoRepositoryQuery {
	
	public List<ProdutoPreco> produtoPorCategoria(Categoria categoria);

	public ProdutoPreco produtoQrcode(int code);
}
