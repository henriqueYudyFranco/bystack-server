package com.projeto.bystack.domain.resource;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.bystack.domain.model.Cliente;
import com.projeto.bystack.domain.repository.ClienteRepository;

@RestController
@RequestMapping(value = "/cliente") 
public class ClienteResource {
	@Autowired
	private ClienteRepository clienteRepository;

	@CrossOrigin(origins = "*")
	@PostMapping(path = "/add")
	public ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cliente) {

		Cliente newCliente = clienteRepository.save(cliente);
		return ResponseEntity.status(HttpStatus.CREATED).body(newCliente);

	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getall")
	public ResponseEntity<List<Cliente>> getClientes() {
		List<Cliente> lstCliente = clienteRepository.findAll();

		if (lstCliente.isEmpty()) {
			return new ResponseEntity<List<Cliente>>(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<List<Cliente>>(lstCliente, HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Cliente> getCliente(@PathVariable("id") long id) {

		Cliente cliente = clienteRepository.findOne(id);

		if (cliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Cliente>(HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@GetMapping(path = "/getcpf"
			+ "/{cpf}")
	public ResponseEntity<Cliente> getClinteByCpf(@PathVariable("cpf") String cpf){
		
		Cliente clienteCpf = clienteRepository.buscarPorCpf(cpf);
		
		if(clienteCpf == null ) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Cliente>(clienteCpf ,HttpStatus.OK);
	}

	@CrossOrigin(origins = "*")
	@PutMapping(path = "/update/{id}")
	public ResponseEntity<Cliente> updateCliente(@PathVariable("id") long id, Cliente cliente) {

		Cliente updateCliente = clienteRepository.findOne(id);

		if (updateCliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}

		BeanUtils.copyProperties(cliente, updateCliente, "id");
		clienteRepository.save(updateCliente);
		return new ResponseEntity<Cliente>(updateCliente, HttpStatus.OK);
	}
	
	@CrossOrigin(origins = "*")
	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<Cliente> deleteCliente(@PathVariable("id") long id){
		
		Cliente deleteCliente = clienteRepository.findOne(id);
		
		if(deleteCliente == null) {
			return new ResponseEntity<Cliente>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Cliente>(HttpStatus.NO_CONTENT);
	}
}
