package com.projeto.bystack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BystackApplication {

	public static void main(String[] args) {
		SpringApplication.run(BystackApplication.class, args);
	}

}
